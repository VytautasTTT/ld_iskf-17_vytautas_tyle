
#define A 3
#define B 4
#define C 5
#define D 6
#define E 7
#define F 8
#define G 9
#define CA1 13
#define CA2 12
const int segs[7] = { A, B, C, D, E, F, G };
const byte numbers[10] = { 0b1000000, 0b1111001, 0b0100100, 0b0110000, 0b0011001, 0b0010010,
0b0000010, 0b1111000, 0b0000000, 0b0010000 };
const int pinTemp = A0; 
void setup() 
{
  pinMode(A, OUTPUT);
  pinMode(B, OUTPUT);
  pinMode(C, OUTPUT);
  pinMode(D, OUTPUT);
  pinMode(E, OUTPUT);
  pinMode(F, OUTPUT);
  pinMode(G, OUTPUT);
  pinMode(CA1, OUTPUT);
  pinMode(CA2, OUTPUT);
   Serial.begin(9600); 
}

void loop() 
{
  int digit1;
  int digit2;
  for (digit1=0; digit1 < 10; digit1++) {
    for ( digit2=0; digit2 < 10; digit2++) {
      unsigned long startTime = millis();
      for (unsigned long elapsed=0; elapsed < 600; elapsed = millis() - startTime) {
        lightDigit1(numbers[digit1]);
        delay(5);
        lightDigit2(numbers[digit2]);
        delay(5);
      }
      if(digit1==1 &&  digit2==5)
      break;
    } 
    if(digit1==1 &&  digit2==5)
      break;
  }
  float volt = analogRead(pinTemp);    //Read the analog pin
  volt = volt* 0.004882814;   // convert output (mv) to readable celcius
  float Ce = (volt - 0.5) * 100.0;
  Serial.print("Temperature: ");
  Serial.print(Ce);
  Serial.println("C");  //print the temperature status
  delay(1000); 
}

void lightDigit1(byte number) 
{
  digitalWrite(CA1, LOW);
  digitalWrite(CA2, HIGH);
  lightSegments(number);
}

void lightDigit2(byte number) 
{
  digitalWrite(CA1, HIGH);
  digitalWrite(CA2, LOW);
  lightSegments(number);
}

void lightSegments(byte number) 
{
  for (int i = 0; i < 7; i++) 
  {
    int bit = bitRead(number, i);
    digitalWrite(segs[i], bit);
  }
}